

## conda et python selon LMD

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x ./Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh

source .bashrc

conda install netcdf4 h5py jinja2 pip sqlalchemy basemap matplotlib numpy scipy


## Jupyter notebooks

sudo apt install -y python3-notebook jupyter jupyter-core python-ipykernel 

## Environnement : ajouter à la fin de .bashrc, pour éviter d'avoir python toujours actif

conda deactivate 
python se réactive alors avec

conda activate
ou

conda activate python2

## ferret install from conda

# https://github.com/NOAA-PMEL/PyFerret/blob/master/README.md

conda create -n FERRET -c conda-forge pyferret ferret_datasets --yes


Pour passer en mode pyferret :

conda activate FERRET

## acroread 

Installation acrobat reader fournie par  https://linuxconfig.org/how-to-install-adobe-acrobat-reader-on-ubuntu-18-04-bionic-beaver-linux

sudo  apt install -y libxml2:i386 gdebi-core
wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
sudo gdebi AdbeRdr9.5.5-1_i386linux_enu.deb

## rstudio, for tuning tools

sudo apt install r-base
wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.5001-amd64.deb
sudo gdebi rstudio-1.2.5001-amd64.deb

sudo apt install -y r-cran-rgl libx11-dev libglu1-mesa-dev libfreetype6-dev

Lancer ensuite la commande "R" (sudo R) pour installer les paquet nécessaires avec l'instruction :

install.packages(c("ncdf4","rstan","tensor","Hmisc","lhs","fields","rgl","shape","mco","far","DiceKriging","GenSA","mvtnorm","loo"))

## Installation LMDZ

wget  http://www.lmd.jussieu.fr/~lmdz/pub/install_lmdz.sh
chmod +x install_lmdz.sh
./install_lmdz.sh 


pour tester si cela fonctionne

cd LMDZtrunk/modipsl/modeles/LMDZ/BENCH32x32x39
conda activate FERRET
ferret
use histday.nc 
shade tsol ; go land 


