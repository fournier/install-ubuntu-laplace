# Pour éviter les effacements abusifs

Editer le fichier ~/.bashrc et placer à la fin les lignes suivantes :

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Quelques scripts bash supplementaires 


git clone https://github.com/alexanderepstein/Bash-Snippets

cd Bash-Snippets

sudo ./install.sh





