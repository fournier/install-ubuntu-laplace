# Général

Remarque : Les adresses des serveurs de paquets sont à modifier dans /etc/apt/sources.list si besoin

# Au premier lancement

sudo apt-get update
sudo apt-get upgrade

# Installation de nouveaux paquets

## Les éditeurs

sudo apt-get install vim emacs retext texmaker texstudio
sudo apt-get install vim-addon-manager vim-latexsuite
sudo vim-addons -w install latex-suite  (pour desttiver : sudo vim-addons -w remove latex-suite )

## Les gestionnaires de version

sudo apt-get install git tig git-lfs

## Ecriture

sudo apt-get install texlive-full pandoc 
sudo apt-get install aspell-fr

## Noweb

sudo apt-get install noweb nowebm

## Graphisme

sudo apt-get install xfig transfig xfig-doc xfig-libs latexdraw gimp inkscape

## Visualisation de données

sudo apt-get install gnuplot gnuplot-x11 paraview

## Calcul scientifique de débutant

sudo apt-get install scilab

## Calcul scientifique

sudo apt-get install g++ cmake cmake-curses-gui gfortran gsl-bin gsl-ref-html libgsl-dev libgsl-dbg libopenexr-dev libopenexr22 openexr openexr-viewers flex bison oprofile cgdb libnetcdf-dev libopenmpi-dev libsdl1.2-dev


## Package supplemenataire pour LMDZ et LMD de facon generale

sudo apt-get install lftp synaptic gdebi grads imagemagick gv grace cdo nco netcdf-bin libnetcdf-dev ncview subversion gifsicle dos2unix pdftk



## Calcul formel

sudo apt-get install maxima xmaxima wxmaxima

## Statistiques

sudo apt-get install r-base r-base-dev

## Linux

sudo apt-get install sshfs rar unrar ark schroot nodejs npm net-tools tmate tmux curl


## outils divers


sudo apt-get install kontact scrcpy openvpn neofetch screen most tree libnotify-bin nmap glances htop asciidoc rename

sudo apt-get install asciinema

sudo apt-get install xournal

sudo apt-get install jpegoptim

sudo apt-get install lm-sensors inxi

sudo apt-get install feh speedtest-cli

sudo apt-get install scdoc

sudo apt-get install mupdf


## Freecad
    
sudo apt install freecad
sudo apt install netgen
sudo sh -c 'echo /usr/lib/x86_64-linux-gnu/netgen > /etc/ld.so.conf.d/netgen.conf'
sudo ldconfig

##  Le client TigerVNC : pour le partage d'écran 

sudo apt-get install tigervnc-viewer 

## pdfgrep

sudo apt-get install pdfgrep


