Installation de l'agent d'inventaire OCS Inventory. Cette installation est pour le LAPLACE seulement. Elle est gérée par le service informatique.

## Installation et configuration

sudo apt-get update
sudo apt-get install ocsinventory-agent 

Prendre l’option http (permet d’indiquer ip du serveur OCS)
pour nous c’est http://130.120.97.13/ocsinventory
le fichier de config est créé
/etc/ocsinventory/ocsinventory-agent.cfg 

## Lancement de l'agent

sudo ocsinventory-agent -t TAG_NAME http://130.120.97.13/ocsinventory

TAG_NAME représente le nom du groupe le batiment et le type de personnel

Dans notre cas 

sudo ocsinventory-agent -t GREPHE_3R1_STAG1 http://130.120.97.13/ocsinventory

