# Serveur ssh

## Installation du serveur

sudo apt-get install openssh-server

## Relancer le serveur

sudo systemctl restart ssh

## Interdiction des connexions avec passwd

Dans /etc/ssh/sshd_config :
  PasswordAuthentication no

## Autorisation des connexions avec clé publique

Dans /etc/ssh/sshd_config :
  PubkeyAuthentication yes

## Rajouter des clés publiques

Les recopier avec cat à la fin du fichier ~/.ssh/authorized_keys

## connexion avec renvoie de Display

ssh -X user@nom_machine

pour lancer un gnome-terminal a distance : 
gnome-terminal  --disable-factory


