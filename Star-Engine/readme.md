# Téléchargement du star-engine

Sous https://www.meso-star.com/projects/star-engine.html télécharger le binaire GNU/Linux et les Sources. On trouve ces fichiers sous ~/Téléchargement :

Star-Engine-0.6.0-GNU-Linux64.tar.gz
Star-Engine-0.6.0-Source.zip

On extrait ces fichiers :

tar -zxvf Star-Engine-0.6.0-GNU-Linux64.tar.gz
unzip Star-Engine-0.6.0-Source.zip

Puis on les déplace sous /opt/Star-Engine :

mkdir /opt/Star-Engine
sudo mv Star-Engine-0.6.0-GNU-Linux64 /opt/Star-Engine/.
sudo mv Star-Engine-0.6.0-Source /opt/Star-Engine/.

# Pour sélectionner la bonne version de star-engine dans chaque session

Editer le fichier ~/.bashrc et placer à la fin la ligne suivante :

export STAR_ENGINE=/opt/Star-Engine
source /opt/Star-Engine/Star-Engine-0.6.0-GNU-Linux64/etc/star-engine.profile

Ces deux lignes seront executée lors de l'ouverture de chaque nouvelle session. Si on veut les executer dans la session courante (la première fois ou après modification), executer la commande suivante :

source ~/.bashrc

# Installation de star-4v_s pour tester la bonne installation de star-engine

Créer un répertoire de travail :

mkdir ~/Travail
cd ~/Travail

Télécharger star-4v_s sur le serveur git :

git clone https://gitlab.com/meso-star/star-4v_s.git
cd star-4v_s
mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=/opt/Star-Engine/Star-Engine-0.6.0-GNU-Linux64 ../cmake
make

Pour tester, télécharger d'abbord une géométrie, par exemple :

cd ~/Travail/star-4v_s/build
wget http://www.prinmath.com/csci5229/OBJ/bunny.zip
unzip bunny.zip
rm bunny.zip

Tester :

./s4vs bunny.obj 10000 1.

Le résultat devrait être :

./s4vs bunny.obj 10000 1.
   4V/S = 1.03775 ~ 1.03985 ± 0.00735101
   # failures: 0/10000

