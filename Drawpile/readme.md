# Drawpile

sudo apt-get install flatpak
     flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
     flatpak install flathub net.drawpile.drawpile
     
Désinstallation : 
     flatpak uninstall net.drawpile.drawpile

# Utilisation :

 a - Un utilisateur créé un tableau :
 
     Cliquer sur l'onglet session puis host et tout en bas entrer 
     91.234.195.167 dans la case remote.
     Ensuite le serveur demandera un username : taper celui de votre choix.
     
 b - Les autres rejoignent ce tableau :
 
     Cliquer sur l'onglet sesssion puis join et tout en bas entrer l'host 91.234.195.167
     
 c - Remarques : 
 
 Ca vaut le coup de prendre 5 min à la première utilisation pour régler son pinceau.
 
 Il est possible de copier-coller des images sur le tableau.
 
 Dans les outils, l'outil "inspector" associé à un symbole bleu avec un point d'intérogation 
 au centre, permet en cliquant sur une zone du tableau de savoir qui a gribouillé.

